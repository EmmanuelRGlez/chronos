<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'WPChronos');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '22xlaNMv7LPNsqDHeh2do8pd8iP8yWaYDRlKbAKCpRhjBpF5xZ4pTUTcJnHmVRjq');
define('SECURE_AUTH_KEY',  'pxzrIgSLsD71m4Ua4amtQzjSsiaAvRxSOdirAVfVvzbsW5elMmAj1RcMxpX89Fj5');
define('LOGGED_IN_KEY',    'InVosrhBHcB2HfInZgB1Zg3Ps2XUxnzl0BR8bnl1CA5rrb2na3Pgi68FQzYjX3xs');
define('NONCE_KEY',        '54BqBgxEZquEPFLcmKoUiQlEDTwO36gwUtCjcLEcIWs1m5HwuzIHvGJToaCit8at');
define('AUTH_SALT',        'syDw0JDVliMRIi3EcEJbKhfjYUShZ1Dq5QA2A70vPFn0HJfNKWfBCD2ucbS4GVa8');
define('SECURE_AUTH_SALT', 'RT1G6QzoKQrFn9yJtXCOkBJPu41lo9F4FGKfnmPWWKPQr0GcVaqYEX1reEKCzsyo');
define('LOGGED_IN_SALT',   'bihTBJYXHpwKokPUO4zbagiaQ0LbV94IdBPqES8S8bm8hKw4j2q98MXEoDB6mX0S');
define('NONCE_SALT',       'KMlpx68une0xThv7loeFuXJYbYN9BhqYRS1epQLUf4pG8Zk4cuQSIUomFgzBG7si');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
